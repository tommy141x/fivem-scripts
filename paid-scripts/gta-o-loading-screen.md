---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00002.jpg
coverY: 0
---

# GTA:O Loading Screen

{% embed url="https://www.youtube.com/watch?v=lHXyHgpVTig" %}
OBS decided to record discord audio so that's in the background of the preview lmao
{% endembed %}

[FiveM Post](https://forum.cfx.re/t/paid-gta-online-loading-done-right/4938272) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: No

#### Requirements: None

### Features

* Rockstar Games Style Intro: The loading screen starts with a Rockstar Games style intro, which can be customized by replacing the logos.
* Full Configurability: Every text, image, and other elements of the loading screen can be easily configured to fit your server's theme.
* Slideshow or YouTube Video Background: You can choose to have a slideshow of images or a YouTube video as the background of the loading screen.
* Background Music: You can add background music to enhance the loading screen experience.

### Configuration

The Tommy’s GTA Online Style Loading Screen script can be fully configured to customize the content and appearance of the loading screen. The configuration options are as follows:

#### Flash Screens

The flash screens are short introductory messages displayed at the beginning of the loading screen. You can customize the text for each flash screen:

1. **First Flash Screen**: `flash1Text`
   * Description: The text displayed on the first flash screen.
   * Example: "Created for roleplayers by roleplayers, we offer a handful of new and exciting features to the game."
2. **Second Flash Screen**: `flash2Text`, `flash2Text1`, `flash2Text2`, `flash2Text3`
   * Description: The texts displayed on the second flash screen.
   * Example:
     * `flash2Text`: "Founded on the basis of everyone deserves to build the community together without an overbearing leadership. YOU are who makes this community matter and are welcome to go beyond your normal routines in roleplay. Be sure to hop in our discord community, and most importantly, have fun!"
     * `flash2Text1`: "Please do not quit or turn off your system while the save icon is showing."
     * `flash2Text2`: "Please do not quit or turn off your system while the loading icon is showing."
     * `flash2Text3`: "Please do not quit or turn off your system while the FiveM cloud save icon is showing."

#### Loading Screen

The loading screen is the main screen displayed during the loading process. You can customize various elements of the loading screen:

* **Logo**: You can configure the usage and appearance of the logo.
  * `useLogo`: Specifies whether to use the logo on the loading screen (`true` or `false`).
  * `useSmallLogo`: Specifies whether to use the small or large logo for the loading screen (`true` or `false`).
  * `logoMaxHeight`: Controls the size of the logo by adjusting its maximum height.
  * `logoPadding`: Defines the spacing between the logo and the bottom left corner of the loading screen.
* **Loading Text**: The text displayed during the loading process.
  * `loadingText`: Specifies the loading text.
* **Slides**: The slideshow of slides displayed on the loading screen. Each slide can be customized with a title, subtitle, description, and image.
  * `slides`: An array of slide objects.
    * `title`: The title of the slide.
    * `subtitle`: The subtitle of the slide.
    * `desc`: The description of the slide.
    * `image`: The filename of the image in the "images" folder.

#### Loading Screen Background

The loading screen background can be configured to use a YouTube video or a slideshow of images:

* **Background Type**: Specify whether to use a YouTube video or a slideshow.
  * `background`: Set to a YouTube video ID (e.g., "Iggl9ymglXw") or leave it as "slideshow" for the images.
* **Slideshow**: If using a slideshow background, you can specify the filenames of the images to be displayed.
  * `backgroundImages`: An array of image filenames in the "images" folder.
