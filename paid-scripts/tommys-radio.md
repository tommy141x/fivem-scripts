---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00006.jpg
coverY: 0
---

# Tommy's Radio

{% embed url="https://www.youtube.com/watch?v=xmhrP7iC6e4" %}

[FiveM Post](https://forum.cfx.re/t/paid-qbcore-tommys-radio/4899654) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: No

#### Requirements: QBCore, PMA Voice

#### Features

Tommy's Radio offers the following features, most of which can be configured according to your preferences:

* Arrow Key Controls: Allows for movement and driving while using the radio.
* Encrypted Signals: Signals are encrypted based on the player's job, adding an extra layer of security.
* Callsign Display: The UI shows the player's callsign, providing identification.
* Configurable Colors: Easily customize the colors of the radio UI to match your desired style.
* Easy Configuration and Usage: The script is designed to be user-friendly and straightforward to configure.

#### Configuration

To customize the behavior and appearance of Tommy's Radio, you can modify the configuration options provided in the script:

```lua
Config = {} -- Tommy's Radio Version 1.0 (QB-CORE)
--[[
████████╗░█████╗░███╗░░░███╗███╗░░░███╗██╗░░░██╗░██████╗
╚══██╔══╝██╔══██╗████╗░████║████╗░████║╚██╗░██╔╝██╔════╝
░░░██║░░░██║░░██║██╔████╔██║██╔████╔██║░╚████╔╝░╚█████╗░
░░░██║░░░██║░░██║██║╚██╔╝██║██║╚██╔╝██║░░╚██╔╝░░░╚═══██╗
░░░██║░░░╚█████╔╝██║░╚═╝░██║██║░╚═╝░██║░░░██║░░░██████╔╝
░░░╚═╝░░░░╚════╝░╚═╝░░░░░╚═╝╚═╝░░░░░╚═╝░░░╚═╝░░░╚═════╝░

██████╗░░█████╗░██████╗░██╗░█████╗░
██╔══██╗██╔══██╗██╔══██╗██║██╔══██╗
██████╔╝███████║██║░░██║██║██║░░██║
██╔══██╗██╔══██║██║░░██║██║██║░░██║
██║░░██║██║░░██║██████╔╝██║╚█████╔╝
╚═╝░░╚═╝╚═╝░░╚═╝╚═════╝░╚═╝░╚════╝░
--]]

-- Change the color of the radio UI. (MUST BE A 6 DIGIT HEX CODE Eg. #FFFFFF, NOT #FFF)
Config.uiColor = '#ffffff'
Config.bgColor = '#4287f5'

-- Make certain channels restricted to certain jobs.
Config.RestrictedChannels = {
    [1] = {
        police = true,
        ambulance = true
    },
    [2] = {
        police = true,
        ambulance = true
    },
}

-- Change the maximum amount of frequencies. (CANNOT BE LARGER THAN 999)
Config.MaxFrequency = 500

-- Change the language of the radio notifications.
Config.messages = {
    ["not_on_radio"] = "Currently Not Connected.",
    ["on_radio"] = "Already Connected.",
    ["restricted_channel_error"] = "Signal is Encrypted.",
    ["invalid_radio"] = "Frequency Unavailable.",
    ["you_leave"] = "Disconnected.",
    ["joined_to_radio"] = "Connected To: ",
}
```

**Configuration Details**

* `Config.uiColor`: Specifies the color of the radio UI. It should be provided as a 6-digit hex code (e.g., `#ffffff`). This option allows you to customize the appearance of the radio UI according to your preference.
* `Config.bgColor`: Sets the background color of the radio UI. Similar to `Config.uiColor`, this option should be provided as a 6-digit hex code.
* `Config.RestrictedChannels`: Defines the channels that are restricted to specific jobs. You can configure which jobs can access certain channels by modifying this option. The example provided demonstrates two channels (`[1]` and `[2]`) that are restricted to the police and ambulance jobs.
* `Config.MaxFrequency`: Specifies the maximum number of frequencies available. This value should not exceed 999.
* `Config.messages`: Allows you to change the language of the radio notifications. Customize the messages based on your preferred language or style.

Feel free to adjust these configuration options as needed to match your server's requirements and desired user experience.

**Note:** It's essential to follow the instructions provided in the comments of the configuration file and ensure that the values are correctly formatted and within the allowed limits.
