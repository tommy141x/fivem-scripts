---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00003.jpg
coverY: 0
---

# Tommy's HUD (Legacy V1)

{% embed url="https://www.youtube.com/watch?v=_8pAM9eRdBM" %}

[FiveM Post](https://forum.cfx.re/t/paid-qbcore-tommys-hud/4890010/) | [Store Page](https://tommy141x.tebex.io/)

**Escrow Encrypted: No**

**Requirements: QBCore**

### Features

* Notification System
  * Modern animated notifications with different icons & sound effects.
  * Can easily be integrated into any resource with the client event.
* Priority Status System
  * Similar to my Priority Status script, this system allows for the current priority status to be shown in-game through the HUD.
  * Admins & Users with the police job will have access to the following commands with this system enabled.
* Postals Display
  * If enabled in the configuration, shows postals in the street display using the postals.json file.
* PMA Voice Currently Talking Radio Display
  * When enabled, users can see who is currently talking on the radio at the top right.

<figure><img src="../.gitbook/assets/9311ba86ebe869f5387915b73536626d5dba83c8.jpeg" alt=""><figcaption></figcaption></figure>

<div>

<figure><img src="../.gitbook/assets/dba11eecb869d1fbf1bced4167ec6ab249c32a87.png" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/fb8af4150f9db0a604d53cf7835ecbc29d697258 (1).png" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/e5241598776a980a2541807abe1e9b1635a75389 (2).png" alt=""><figcaption></figcaption></figure>

</div>

### Configuration

The configuration file `config.lua` contains various options to customize the behavior and appearance of the HUD. Here's an in-depth explanation of each option:

#### Core Settings

* `Config.coreName`: The name of the QB-Core resource. Change this to match the name of your QB-Core resource. Example: `"qb-core"`
* `Config.removeMinimapHealthBarsAndBorder`: When set to `true`, it removes the health bars and border from the minimap. Set it to `false` if you want to keep the default minimap appearance.
* `Config.uiColor`: The main color of the UI in hex format. Example: `"#fff"`
* `Config.accentColor`: The accent color of the UI in hex format. Example: `"#828181"`
* `Config.loggedIn`: A function that determines whether the HUD should be displayed for a player. Modify this function to suit your server's logic. By default, the HUD is shown when `PlayerData.LoggedIn` is `true`.

#### Bottom Left Section

* `Config.usePostals`: When set to `true`, it shows postals in the street display using the `postals.json` file. Make sure to place the `postals.json` file in the resource folder.
* `Config.usePrioStatus`: When set to `true`, enables the priority status feature. If set to `false`, the HUD will show date/time instead of priority status.
* `Config.prioCooldownTime`: The length of the priority cooldown in minutes. This option is only relevant if the priority status feature is enabled.
* `Config.noShootingOnPeacetime`: When set to `true`, players are not allowed to attack or shoot during peacetime. This option is only relevant if the priority status feature is enabled.

#### Bottom Right Section

* `Config.UseMPH`: When set to `true`, the speedometer will display speed in miles per hour (MPH). Set it to `false` to display speed in kilometers per hour (KMH).
* `Config.showSpeedometer`: When set to `true`, the speedometer will be displayed. Set it to `false` to hide the speedometer.
* `Config.getFuelFunction`: A function that determines how fuel level is retrieved for the fuel indicator. Modify this function to match your fuel system. By default, it uses the `LegacyFuel` resource.

#### Top Right Section

* `Config.showInfo`: When set to `true`, the top right section of the HUD (info section) will be displayed. Set it to `false` to hide the info section.
* `Config.serverName`: The text shown at the top right (info section). Example: `"myroleplayserver.net"`. This can be your server's name or any custom text.
* `Config.infoIcon`: The URL to an image that will be shown as an icon at the top right. Example: `"https://i.imgur.com/MF9gM6z.png"`. Provide the URL of the desired image.
* `Config.usePMAVoice`: When set to `true`, if using PMA Voice, it shows the currently talking players on the connected radio channel at the top right. It will show the callsign if available.

```lua
Config = {} -- Tommy's HUD for QB-Core Version 1.0
--[[
████████╗░█████╗░███╗░░░███╗███╗░░░███╗██╗░░░██╗██╗░██████╗      ██╗░░██╗██╗░░░██╗██████╗░
╚══██╔══╝██╔══██╗████╗░████║████╗░████║╚██╗░██╔╝╚█║██╔════╝      ██║░░██║██║░░░██║██╔══██╗
░░░██║░░░██║░░██║██╔████╔██║██╔████╔██║░╚████╔╝░░╚╝╚█████╗░      ███████║██║░░░██║██║░░██║
░░░██║░░░██║░░██║██║╚██╔╝██║██║╚██╔╝██║░░╚██╔╝░░░░░░╚═══██╗      ██╔══██║██║░░░██║██║░░██║
░░░██║░░░╚█████╔╝██║░╚═╝░██║██║░╚═╝░██║░░░██║░░░░░░██████╔╝      ██║░░██║╚██████╔╝██████╔╝
░░░╚═╝░░░░╚════╝░╚═╝░░░░░╚═╝╚═╝░░░░░╚═╝░░░╚═╝░░░░░░╚═════╝░      ╚═╝░░╚═╝░╚═════╝░╚═════╝░
█▀▀   █▀█   █▀█       █▀█   █▄▄   ▄▄   █▀▀   █▀█   █▀█   █▀▀
█▀░   █▄█   █▀▄       ▀▀█   █▄█   ░░   █▄▄   █▄█   █▀▄   ██▄
--]]
-- EVERYTHING
Config.coreName = "qb-core" -- This is called here: local QBCore = exports['CORENAME']:GetCoreObject()
Config.removeMinimapHealthBarsAndBorder = true -- This is pretty self explanatory.
Config.uiColor = "#fff" -- The main color of the UI in hex format. (Default: #fff)
Config.accentColor = "#828181" -- The accent color of the UI in hex format. (Default: #828181)
Config.loggedIn = function (PlayerData)
    return (PlayerData.metadata ~= nil) -- Temporary Fix for QBCore not returning the correct value (PlayerData.LoggedIn)
    --return true -- Incase your having issues with the HUD not updating
    --return PlayerData.LoggedIn -- Hud only shows when this value is true
end

-- BOTTOM LEFT
Config.usePostals = true -- Shows postals in the street display using the postals.json file set in fxmanifest.lua.
Config.usePrioStatus = true -- Use of priority status feature, will show date/time instead if set to false.
Config.prioCooldownTime = 15 -- Length of priority cooldown in minutes (if the priority status feature is enabled).
Config.noShootingOnPeacetime = false -- This won't allow players to attack or shoot during peacetime (if the priority status feature is enabled).

-- BOTTOM RIGHT
Config.UseMPH = true -- Whether to use MPH(true) or KMH (false).
Config.showSpeedometer = true -- Whether to show the spedometer or not.
Config.getFuelFunction = function (vehicle)
    -- return 100 * GetVehicleFuelLevel(vehicle) / GetVehicleHandlingFloat(vehicle, "CHandlingData", "fPetrolTankVolume") -- Non Legacy Fuel
    return exports["LegacyFuel"]:GetFuel(vehicle) -- Allows correct fuel level to be shown for legacy fuel.
end

-- TOP RIGHT
Config.showInfo = true -- Whether to show the top right section of the hud (info section)
Config.serverName = "myroleplayserver.net" -- Text shown at the top right (info section) [Soon Feature will provide option to show job name in place of this.]
Config.infoIcon = "https://i.imgur.com/MF9gM6z.png" -- URL to any image to be shown as an icon at the top right.

-- This will still work even if the info section is disabled. (Config.showInfo = false or true)
Config.usePMAVoice = false -- If using PMA Voice, this shows currently talking players on the connected radio channel at the top right. (Will show callsign if available.)

```

### More Information

While self-hosting the server and testing the script, the resmon value has stayed under 0.05ms.\
\
**Priority Status Commands:**

> **Set the priority status:** /setp \<inactive,active,cooldown,peacetime> \<optional notes>\
> **Update or set priority notes:** /setn \<notes>

**Notification Events:**

```lua
TriggerEvent('hud:client:notify', 'success', "Notification Title", "My cool message!")
TriggerEvent('hud:client:notify', 'info', "Hey!", "You have $20.")
TriggerClientEvent('hud:client:notify', source, 'error', "Wrong", "You messed up!")
```

### Resolution Configuration

Adding support for more resolutions goes as follows and is repeatable for multiple resolution support. (Haven't had time to update the script)

Add the following to `index.html` under the `</body>` line:

```javascript
<script>
 $('document').ready(function(){
  if (screen.height > 1080) {
   $("#hud").removeClass("hud");
   $("#hud").addClass("hud-customres1");
  }
 });
</script>
```

Add the following inside the `styles.css` file:

```css
.hud-customres1 {
    filter: drop-shadow(0 0.2rem 0.25rem rgba(0,0,0,0.5));
    width: 420px;
    padding: 20px 20px;
    position: absolute;
    bottom: 15px;
    left: 100px;
    overflow: hidden;
}
```

You can modify the `bottom` and `left` values within `.hud-customres1` to get the correct hud positioning for the chosen resolution.

### Updates

* Fixed Fuel Number Flickering
* Added Ability to Change Fuel Function
* Added Ability to Change LoggedIn Function

### To-Do List

* This project is no longer receiving support and is now abandoned.
* This project has also been made open source.

***

**Note**: The documentation provided here is intended to help you understand and configure Tommy's HUD script effectively.
