---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00005.jpg
coverY: 0
---

# Tommy's POV

{% embed url="https://www.youtube.com/watch?v=UXOCkk6myoQ" %}

[FiveM Post](https://forum.cfx.re/t/tommys-pov-a-realistic-pov-script/5039367) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: No

**Requirements: None**

#### Configuration

The configuration options for Tommy's POV script are as follows:

```lua
--[[
  ______                               _          ____  ____ _    __
 /_  __/___  ____ ___  ____ ___  __  _( )_____   / __ \/ __ \ |  / /
  / / / __ \/ __ `__ \/ __ `__ \/ / / /// ___/  / /_/ / / / / | / /
 / / / /_/ / / / / / / / / / / / /_/ / (__  )  / ____/ /_/ /| |/ /
/_/  \____/_/ /_/ /_/_/ /_/ /_/\__, / /____/  /_/    \____/ |___/
                              /____/
-- Developed by https://github.com/tommy141x ]]

-- Command that toggles the POV
commandName = "pov"

-- Camera FOV
camFOV = 80.0

-- Camera offset from the player's body
camOffsetX = 0.05
camOffsetY = 0.05
camOffsetZ = 0.15

-- Camera offset from the player's body when reversing in a vehicle and holding C (reverse camera)
reverseCamOffsetX = -0.29
reverseCamOffsetY = -0.29
reverseCamOffsetZ = 0.2
```

**Configuration Details**

* `commandName`: Specifies the command used to toggle the first-person point of view.
* `camFOV`: Sets the camera field of view (FOV) for the first-person view. Higher values provide a wider view, while lower values create a more zoomed-in effect.
* `camOffsetX`, `camOffsetY`, `camOffsetZ`: Defines the camera offset from the player's body in the regular view. These values determine the position of the camera relative to the player's character. Adjusting these offsets allows you to fine-tune the camera position and achieve the desired visual representation.
* `reverseCamOffsetX`, `reverseCamOffsetY`, `reverseCamOffsetZ`: Defines the camera offset from the player's body when reversing in a vehicle and holding the C key (reverse camera). These offsets control the camera position specifically for the reverse view in vehicles.

**Understanding Camera Offsets**

Camera offsets determine the position of the camera relative to the player's body. Each offset represents a specific direction:

* `camOffsetX`: Controls the horizontal position of the camera. A positive value moves the camera to the right, while a negative value moves it to the left.
* `camOffsetY`: Controls the vertical position of the camera. A positive value raises the camera, while a negative value lowers it.
* `camOffsetZ`: Controls the depth position of the camera. A positive value moves the camera away from the player's body, while a negative value brings it closer.

By adjusting these offsets, you can customize the camera position to your liking, ensuring a realistic and immersive first-person experience.

**Note:** It's recommended to experiment with different offset values to find the configuration that suits your preferences and gameplay style.
