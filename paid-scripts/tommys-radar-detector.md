---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00004.jpg
coverY: 0
---

# Tommy's Radar Detector

{% embed url="https://www.youtube.com/watch?v=GPMLoupUSYk" %}

[FiveM Post](https://forum.cfx.re/t/paid-standalone-qbcore-youniden-radar-detector/4929675) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: Yes

#### Requirements: Wraith ARS 2X

Features:

* Works with Wraith ARS 2X and can be configured to work with other frameworks.
* Displays the direction and strength of radar signals.
* The UI can be easily resized and moved, with individual positions saved for each player.
* UI resizing and positioning can be done using the /youniden\_layout command, with + and - for size adjustment and mouse for position adjustment.

<div>

<figure><img src="../.gitbook/assets/e070db003fc3aed09b26e92942120412b3a43831.png" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/08c2fb062395a45dbd3d6ec90469f1fc33999243 (1).png" alt=""><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/84888b9c5978e8c1ceebada71f544a8f59f31dd0.png" alt=""><figcaption></figcaption></figure>

</div>

Configuration: **cl\_config.lua:**

This file contains client-side configuration options for the radar detector script.

```lua
Config = {} -- Tommy's Radar Detector Script v1.0

-- The name of the Wraith ARS 2X resource.
Config.wraithResourceName = "wk_wars2x"
-- Choose either 'mph' or 'kph'
Config.speedUnit = "mph"
-- Enable this if you want to use the command to open the radar detector (disable if using it as an item).
Config.useAsCommand = false
Config.commandName = "youniden"
```

* `wraithResourceName`: Specifies the name of the Wraith ARS 2X resource. Make sure to set it correctly based on your resource name.
* `speedUnit`: Allows you to choose the unit of measurement for the radar detector. Options are "mph" or "kph".
* `useAsCommand`: Set this option to true if you want to use the /youniden command to open the radar detector. If using it as an item, set it to false.
* `commandName`: Specifies the command name to open the radar detector if `useAsCommand` is enabled.

**sv\_config.lua:**

This file contains server-side configuration options for the radar detector script.

```lua
-- If using qbcore, enable this to allow the radar detector to be used as an item.
local useAsItem = true
local itemName = "radardetector"
local QBCore = exports['qb-core']:GetCoreObject()

list = {}
CreateThread(function()
    while true do
        Wait(1000)
        TriggerClientEvent('youniden:syncList', -1, list)
    end
end)

if useAsItem then
    QBCore.Functions.CreateUseableItem(itemName, function(source)
        TriggerClientEvent('youniden:radar', source)
    end)
end

RegisterNetEvent('youniden:updateList')
AddEventHandler('youniden:updateList', function(toggle)
    list[source] = toggle
end)
```

* `useAsItem`: Set this option to true if you are using the QBCore framework and want to allow the radar detector to be used as an item. Set it to false otherwise.
* `itemName`: Specifies the name of the radar detector item if `useAsItem` is enabled.
* `QBCore`: Retrieves the core object of the QBCore framework.
* `list`: A table that stores the radar detector toggle status for each player.
* `CreateThread`: A function used to create a thread that continuously updates the radar detector toggle status.
* `TriggerClientEvent`: Sends the radar detector toggle status to all clients.
* `QBCore.Functions.CreateUseableItem`: Creates a usable item for the radar detector if `useAsItem` is enabled.

Additional Instructions:

* For all users using WK\_WARS2X, add the following line to the `fxmanifest.lua` file of the Wraith resource: `export "DetectorExport"`. Additionally, add the provided code anywhere within the `cl_radar.lua` file within the Wraith resource folder.

{% code title="Add Code to cl_radar.lua" %}
```lua
function DetectorExport()
	return RADAR;
end
```
{% endcode %}

* For QBCore users who wish to make this an item, include the provided code in your `qbcore/shared/items.lua` file. Also, make sure to drag the `radardetector.png` file into the `qb-inventory/html/images` directory.

{% code title="QB Core Item" %}
```lua
['radardetector'] = {
  ['name'] = 'radardetector',
  ['label'] = 'Youniden Radar Detector',
  ['weight'] = 3000,
  ['type'] = 'item',
  ['image'] = 'radardetector.png',
  ['unique'] = true,
  ['useable'] = true,
  ['shouldClose'] = true,
  ['combinable'] = nil,
  ['description'] = 'We are not liable for your reckless driving.',
},
```
{% endcode %}

* Finally, enable the "useAsItem" function in the config file to activate the radar detector as an item.

**Please** ensure that you follow these instructions carefully to ensure proper functionality and integration with the Wraith ARS 2X and QBCore frameworks.
