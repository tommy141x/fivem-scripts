---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00001.jpg
coverY: 0
---

# Custom Unit Number

{% embed url="https://www.youtube.com/watch?v=qckcsj-_lqI" %}

[FiveM Post](https://forum.cfx.re/t/paid-custom-unit-number/4808312) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: Yes

#### Requirements: None

### Usage

1. Place both the `uNumberProps` and `uNumber` resources into your server.
2.  Edit your `server.cfg` file to start the resources:

    ```cfg
    start uNumberProps
    start uNumber
    ```
3. Configure the offsets and rotations for each possible digit place that can be shown on the vehicle in the `config.lua` file.
4. Restart the `uNumber` resource as needed to apply the configuration changes.
5.  When in a configured vehicle, use the following commands to set your unit number:

    * `/unitnum <number> <color>`: Sets the unit number with the specified color (black or white).
    * `/unitnum clear`: Clears the unit number from the vehicle.



<figure><img src="../.gitbook/assets/FiveM_b2802_GTAProcess_foUqsV1Bmr.gif" alt=""><figcaption><p>GIF by angrymaximus</p></figcaption></figure>

### Upcoming Features

* Exports for developers to integrate with.
* Ability to change the scale of the unit numbers.
* Possible menu to easily configure the placement of the unit numbers.

### Configuration

The `config.lua` file contains the following configuration options:

```lua
--[[
_   _      _ _
| | | |_ _ (_) |_
| |_| | ' \| |  _|
\___/|_||_|_|\__|
| \| |_  _ _ __ | |__  ___ _ _
| .` | || | '  \| '_ \/ -_) '_|
|_|\_|\_,_|_|_|_|_.__/\___|_|

--]]
-- Developed by https://github.com/tommy141x

-- Command Used To Change Unit Number
commandName = "unitnum"

-- Vehicle Configuration
uNumberVehicles = {
  {
    vehicle = "valor2rb", -- Spawn Code
    boneName = "chassis", -- Bone Name the Numbers are attached to (https://docs.fivem.net/natives/?_0xFB71170B7E76ACBA)
    numPos = {            -- Offset Values (from chassis bone) for the 3 number slots
      {
        posX = -0.4,      -- X Position (left & right)
        posY = -0.75,     -- Y Position (forward & back)
        posZ = 0.76,      -- Z Position (up & down)
        rotX = 6.0,       -- X Rotation (Roll Left & Right)
        rotY = -4.0,      -- Y Rotation (Roll Forward & Back)
        rotZ = 0.0        -- Z Rotation (Spin)
      },
      {
        posX = 0.0,
        posY = -0.75,
        posZ = 0.775,
        rotX = 6.0,
        rotY = 0.0,
        rotZ = 0.0
      },
      {
        posX = 0.4,
        posY = -0.75,
        posZ = 0.76,
        rotX = 6.0,
        rotY = 4.0,
        rotZ = 0.0
      }
    }
  },
  {
    vehicle = "valor5rb",
    boneName = "chassis",
    numPos = {
      {
        posX = -0.4,
        posY = -0.75,
        posZ = 0.86,
        rotX = 3.0,
        rotY = -4.0,
        rotZ = 0.0
      },
      {
        posX = 0.0,
        posY = -0.75,
        posZ = 0.88,
        rotX = 3.0,
        rotY = 0.0,
        rotZ = 0.0
      },
      {
        posX = 0.4,
        posY = -0.75,
        posZ = 0.86,
        rotX = 3.0,
        rotY = 4.0,
        rotZ = 0.0
      }
    }
  }
}
```

* `commandName`: The command used to change the unit number. Example: `"unitnum"`. Players can use this command followed by a number and color to set their unit number.
* `uNumberVehicles`: A table that contains the vehicle configurations. Each entry represents a vehicle that can display unit numbers and contains the following properties:
  * `vehicle`: The spawn code of the vehicle. Example: `"valor2rb"`.
  * `boneName`: The bone name to which the numbers are attached. Reference: [FiveM Natives Documentation](https://docs.fivem.net/natives/?\_0xFB71170B7E76ACBA).
  * `numPos`: An array of offset values for the three number slots. Each entry represents a digit place and contains the following properties:
    * `posX`: The X position (left & right).
    * `posY`: The Y position (forward & back).
    * `posZ`: The Z position (up & down).
    * `rotX`: The X rotation (Roll left & right).
    * `rotY`: The Y rotation (Roll forward & back).
    * `rotZ`: The Z rotation (Spin).

<figure><img src="../.gitbook/assets/8dfa99e42cb843f5d22c8ee9162dc4b0906abb2a.jpeg" alt=""><figcaption></figcaption></figure>

Note: The documentation provided here is intended to help you understand and configure the Custom Unit Number script effectively.
