---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00009.jpg
coverY: 0
---

# Tommy's Discord API

<figure><img src="../.gitbook/assets/Comp 1_00009.jpg" alt=""><figcaption></figcaption></figure>

[FiveM Post](https://forum.cfx.re/t/tommys-discord-api-tool-for-devs/4806569) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: Yes

#### Requirements: None

This tool easily allows for discord integration in scripts.

### Why this API?

There are many resources that already serve a similar purpose, however, the methods in which these resources acquire discord information is not reliable. **Discord Data Saver** stores players' discord IDs and caches role information. No longer will your players have issues with discord permissions.

### How do I use it?

1. Configure the Guild ID & Bot Token in the `config.lua` file.
2. Add the following to your resources `fxmanifest.lua`:

```lua
dependency 'ddSaver'
```

#### Client Exports

```lua
luaCopy codeexports.ddSaver:getRoles() -- Returns a table of role IDs
exports.ddSaver:getId() -- Returns the player's discord ID
exports.ddSaver:refreshData() -- Refreshes the player's discord data
exports.ddSaver:hasRole("roleid") -- Returns true or false
```

#### Server Exports

```lua
luaCopy codeexports.ddSaver:getRoles(source) -- Returns a table of role IDs
exports.ddSaver:getId(source) -- Returns the player's discord ID
exports.ddSaver:refreshData(source) -- Refreshes the player's discord data
exports.ddSaver:hasRole(source, "roleid") -- Returns true or false
```

### Examples

#### `client.lua`

```lua
luaCopy codelocal leoRole = "909620367654543381"

if exports.ddSaver:hasRole(leoRole) then
  print("omg you have the role")
end

RegisterCommand('permcheck', function(source)
  exports.ddSaver:refreshData()
  TriggerEvent("chatMessage", "Discord Permissions", {20, 20, 20}, "Permissions Updated.")
end, false)
```

#### `server.lua`

```lua
luaCopy codeRegisterCommand('printRoles', function(source)
  local roles = exports.ddSaver:getRoles(source)
  if roles then
    for role in pairs(roles) do
      print("Role Found: " .. roles[role])
    end
  end
end, false)
```
