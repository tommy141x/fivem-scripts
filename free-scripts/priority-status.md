---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00008.jpg
coverY: 0
---

# Priority Status

{% embed url="https://www.youtube.com/watch?v=RdS0EO2PaxE" %}

[FiveM Post](https://forum.cfx.re/t/free-priority-status/1929937) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: No

#### Requirements: None

The Priority Status script is a FiveM resource that allows users to set and display their priority status, with options to update notes. This script provides configuration options to customize the priority status settings. The command usage is as follows:

### Command Usage

* `/setp <available, active, hold, pending, cooldown> <notes(optional)>`: Sets the priority status.
* `/setn <notes>`: Updates the notes on the priority status.
* `/setn clear`: Clears the current priority notes.

<figure><img src="../.gitbook/assets/43893c2d138d7eaedaf40c5efadbaa26b64418a0.jpeg" alt="" width="375"><figcaption></figcaption></figure>

### ACE Permissions

To grant users permission to use the priority status commands, assign them the ACE Permission: `prio.status`. This permission ensures that only authorized users can set and update the priority status.

If you have any questions or need further assistance, please don't hesitate to ask. Feel free to contact us at.
