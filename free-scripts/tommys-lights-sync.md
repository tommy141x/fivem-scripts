---
description: Configuration guide, common issues & solutions, code snippets, and more.
cover: ../.gitbook/assets/Comp 1_00007.jpg
coverY: 0
---

# Tommy's Lights Sync

{% embed url="https://www.youtube.com/watch?v=O2i4TbvDXAU" %}

[FiveM Post](https://forum.cfx.re/t/free-light-sync-system/4933713/) | [Store Page](https://tommy141x.tebex.io/)

#### Escrow Encrypted: Yes

#### Requirements: None

Tommy's Light Sync is a FiveM script that synchronizes the lights of emergency vehicles, providing a realistic lighting sequence similar to the real-life bluePRINT Sync system. Please note that this script only syncs the start time of the lighting sequence for each vehicle, so it works best with a vehicle pack where all the vehicles were created by the same developer. This script does not work with ELS (Emergency Lighting System).

### Configuration

In the `config.lua` file, you can customize the settings for Tommy's Light Sync:

```markdown
--[[
Tommy's Lights v1.0

  _______                              _       _      _       _     _
 |__   __|                            ( )     | |    (_)     | |   | |
    | | ___  _ __ ___  _ __ ___  _   _|/ ___  | |     _  __ _| |__ | |_ ___
    | |/ _ \| '_ ` _ \| '_ ` _ \| | | | / __| | |    | |/ _` | '_ \| __/ __|
    | | (_) | | | | | | | | | | | |_| | \__ \ | |____| | (_| | | | | |_\__ \
    |_|\___/|_| |_| |_|_| |_| |_|\__, | |___/ |______|_|\__, |_| |_|\__|___/
                                  __/ |                  __/ |
                                 |___/                  |___/
--]]
-- Developed by https://github.com/tommy141x

refreshRate = 5000 -- How often to sync lights (in ms)
```

* `refreshRate`: Specifies how often the lights should be synchronized, in milliseconds. Adjust this value based on your preference and server performance. The default value is 5000ms (5 seconds).

{% embed url="https://www.youtube.com/watch?v=Kq2tGD_Rdnk" %}

Please note that the script requires the presence of a person in an emergency vehicle at least once during the refresh interval. If you quickly spawn a vehicle and exit it, the lights may not sync until you sit in each vehicle for a few seconds.

If you have any questions or need further assistance, please feel free to ask.
