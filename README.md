---
description: Troubleshooting and FAQ
cover: >-
  https://images.unsplash.com/photo-1515879218367-8466d910aaa4?crop=entropy&cs=srgb&fm=jpg&ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw4fHxjb2RlfGVufDB8fHx8MTY4NzgxMTM0NXww&ixlib=rb-4.0.3&q=85
coverY: 0
---

# FiveM Escrow System

**Important Information Before Making a Purchase**

1. Ensure Correct FiveM Account Usage

> ❗️ Your purchases are linked to your FiveM account, and your FiveM account is linked to your FiveM server license key. Meaning the FiveM account you use to login on Tebex before the purchase needs to be the same FiveM account that you use to log into [https://keymaster.fivem.net](https://keymaster.fivem.net/) to generate your server license key.

1. Minimum Required Server Artifacts Version

> ❗️ It is required that your server is using server artifacts version 4960+.

**Troubleshooting for Non-functioning Resources**

### Issue: Lack of Required Entitlement

*   **Reason 1:** After you purchase or transfer the resource(s), a server restart is always required.

    **Solution:** Simply restart your server.
*   **Reason 2:** Your server is using a FiveM server license key that is not linked to the initial purchase, meaning the resource(s) were purchased on the wrong FiveM account.

    **Solution 1:** Whoever has access to the FiveM account the purchase(s) were made on needs to login to FiveM’s Keymaster and transfer them to the correct account.

    **Solution 2:** If you login to FiveM’s Keymaster and you are able to see your resource(s), this means your server is using a license key that is not linked to your purchase(s). So either create a new license key on Keymaster using this FiveM account or transfer them to the correct account.

    **Solution 3:** If you do not have access or cannot contact the owner of the FiveM account the purchase(s) were made on, contact us in a Management Ticket on our Discord.

    You can contact us in a Management Ticket on our Discord to check which FiveM account your purchase(s) are linked to.
*   **Reason 3:** Because the resource(s) were transferred to you by the original owner, meaning the purchase(s) are still linked to the original owner's FiveM account.

    **Solution 1:** Whoever has access to the FiveM account the purchase(s) are linked to needs to log in to FiveM’s Keymaster and transfer them to the correct account.

    **Solution 2:** If you cannot contact the owner of the FiveM account the purchase(s) are linked to, contact us in a Management Ticket on our Discord.

    You can contact us in a Management Ticket on our Discord to check which FiveM account your purchase(s) are linked to.

Issue: Failed to Verify Protected Resource

*   **Reason 1:** Files were possibly corrupted during transfer.

    **Solution:** Ensure the encrypted files are copied; the .fxap file must be included. Some FTP programs skip these files. If you use FileZilla, try using WinSCP instead.

### Issue: Syntax Error

*   **Reason 1:** A server restart is always required after adding a new resource or updating an existing one.

    **Solution:** Simply restart your server.
*   **Reason 2:** You are not using a compatible server artifacts version - #Minimum server artifacts version.

    **Solution:** Download a newer server artifacts version from [https:/runtime.fivem.net](https://chat.openai.com/runtime.fivem.net) (at least version 4960+).
*   **Reason 3:** You have edited the encrypted code.

    **Solution:** Delete the current folder and redownload a fresh unedited version from FiveM's Keymaster.
*   **Reason 4:** Files were possibly corrupted during transfer.

    **Solution:** Ensure the encrypted files are copied; the .fxap file must be included. Some FTP programs skip these files. If you use FileZilla, try using WinSCP instead.

### Issue: Couldn't Start Resource

*   **Reason 1:** If this only happens to some players but not all of them, this is because the time on your VPS/dedicated server is behind.

    **Solution:** Make sure in your Windows settings, the time is being set automatically. Start > Settings > Time and Language.

**FAQ**

* **I own 3 servers, can I use the same resource on my 3 different servers at the same time?** Yes, as long as you are using a server license key that was generated from your FiveM account. This also applies to dev/local host servers too.
* **Can I transfer the resource to another FiveM account?** Yes, you can transfer an asset 1 time only, and it is permanent, so it cannot be undone. You can log in to FiveM’s Keymaster and transfer them to the correct account. Be aware that transfers are 1 time only.
